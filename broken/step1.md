This is your first step.

## Task

This is an _example_ of creating a scenario and running a **command**

`echo 'Hello World'`{{execute}}

`python`{{execute}}

```
def new_func():
  print('stuff')


```{{execute}}

```
def my_func():
  if (True):
    print('true')
  else:
    print('false')


```{{execute}}

`my_func()`{{execute}}

```
def another_func():
  if (True):
    print('true')
  else:
    print('false')

# execute this function to print "true"
```{{execute}}

`another_func()`{{execute}}
